import React from 'react';
import {View, Text, Button} from 'react-native';
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';

class Logout extends React.Component {
  handleLogout = () => {
    return this.props.screenProps.changeLoginState(false);
  };

  render() {
    const {currentUser} = this.props.data;

    return (
      <View>
        <View>
          {currentUser && (
            <View>
              <Text>{currentUser._id}</Text>
              <Text>{currentUser.email}</Text>
            </View>
          )}
          <Button title="LOG OUT" full onPress={this.handleLogout} />
        </View>
      </View>
    );
  }
}

export default graphql(
  gql`
    query User {
      currentUser {
        _id
        email
      }
    }
  `,
)(Logout);
