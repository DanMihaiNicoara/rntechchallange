import React, {Component} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

class Homepage extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Button
          title="Login"
          onPress={() => this.props.navigation.navigate('Login')}
        />
        <Button
          title="Signup"
          onPress={() => this.props.navigation.navigate('Register')}
        />
        <Button
          title="Sign up with Email"
          onPress={() => this.props.navigation.navigate('Register')}
        />
        <Button
          title="Login"
          onPress={() => this.props.navigation.navigate('Login')}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'orange',
  },
  button: {
    borderRadius: 20,
  },
});
export default Homepage;
