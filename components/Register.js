import React, {Component} from 'react';
import {View, Text, TextInput, Button, StyleSheet} from 'react-native';
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';

class Register extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      emailError: false,
      password: '',
      passwordError: false,
      confirmPassword: '',
      confirmPasswordError: false,
    };
  }

  handleInputChange = (field, value) => {
    const newState = {
      ...this.state,
      [field]: value,
    };
    this.setState(newState);
  };

  handleSubmit = () => {
    const {email, password, confirmPassword} = this.state;
    if (email.length === 0) {
      return this.setState({emailError: true});
    }
    this.setState({emailError: false});

    if (password.length === 0) {
      return this.setState({passwordError: true});
    }
    this.setState({passwordError: false});

    if (confirmPassword.length === 0) {
      return this.setState({confirmPasswordError: true});
    }
    this.setState({confirmPasswordError: false});

    if (password !== confirmPassword) {
      return this.setState({passwordError: true, confirmPasswordError: true});
    }
    this.setState({passwordError: false, confirmPasswordError: false});
    this.props
      .signup(email, password)
      .then(({data}) => {
        console.log(data, 'DATA');
        return this.props.screenProps.changeLoginState(true, data.signup.jwt);
      })
      .then(() => this.props.navigation.navigate('Login'))
      .catch(e => {
        // If the error message contains email or password we'll assume that's the error.
        if (/email/i.test(e.message)) {
          this.setState({emailError: true});
        }
        if (/password/i.test(e.message)) {
          this.setState({passwordError: true});
        }
      });
  };

  render() {
    const {navigation} = this.props;
    const {emailError, passwordError, confirmPasswordError} = this.state;

    return (
      <View>
        <View>
          <View>
            <TextInput
              placeholder="Email"
              onChangeText={value => this.handleInputChange('email', value)}
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
            />
            <TextInput
              placeholder="Password"
              onChangeText={value => this.handleInputChange('password', value)}
              autoCapitalize="none"
              autoCorrect={false}
              secureTextEntry
            />
            <TextInput
              placeholder="Confirm Password"
              onChangeText={value =>
                this.handleInputChange('confirmPassword', value)
              }
              autoCapitalize="none"
              autoCorrect={false}
              secureTextEntry
            />
          </View>
          <Button title="SIGN UP" onPress={this.handleSubmit} />
          <Button
            title="SIGN IN"
            onPress={() => navigation.navigate('Login')}
          />
        </View>
      </View>
    );
  }
}

export default graphql(
  gql`
    mutation {
      signUpUser(data: {email: "dan@fasttrackit.org", password: "123456"}) {
        accessToken
        refreshToken
        user {
          email
        }
      }
    }
  `,
  {
    props: ({mutate}) => ({
      signup: (email, password) => mutate({variables: {email, password}}),
    }),
  },
)(Register);
