import React, {Component} from 'react';
import {View, Text, TextInput, Button, StyleSheet} from 'react-native';
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';

import 'apollo-client';
import 'graphql';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      emailError: false,
      password: '',
      passwordError: false,
    };
  }

  handleInputChange = (field, value) => {
    const newState = {
      ...this.state,
      [field]: value,
    };
    this.setState(newState);
  };

  handleSubmit = () => {
    console.log(this.state,"staere")
    const {email, password} = this.state;
    if (email.length === 0) {
      return this.setState({emailError: true});
    }
    this.setState({emailError: false});

    if (password.length === 0) {
      return this.setState({passwordError: true});
    }
    this.setState({passwordError: false});

    this.props
      .login(email, password)
      .then(({data}) => {
        return this.props.screenProps.changeLoginState(true, data.login.jwt);
      })
      .catch(e => {
        // If the error message contains email or password we'll assume that's the error.
        if (/email/i.test(e.message)) {
          this.setState({emailError: true});
        }
        if (/password/i.test(e.message)) {
          this.setState({passwordError: true});
        }
      });
  };

  render() {
    const {emailError, passwordError} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text>Email</Text>
          <TextInput
            placeholder="Email"
            onChangeText={value => this.handleInputChange('email', value)}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
          />
          <Text>Password</Text>
          <TextInput
            placeholder="Password"
            onChangeText={value => this.handleInputChange('password', value)}
            autoCapitalize="none"
            autoCorrect={false}
            secureTextEntry
          />
          <Button
            title="LOG IN"
            onPress={() => {
              this.handleSubmit();
            }}
          />
          <Text>Forgot my password</Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'orange',
  },
  content: {
    width: '70%',
    margin: 'auto',
  },
  input: {
    borderRadius: 10,
    backgroundColor: 'white',
  },
  button: {
    borderRadius: 20,
  },
});

export default graphql(
  gql`
    mutation login($email: String!, $password: String!) {
      login(email: $email, password: $password) {
        _id
        email
        jwt
      }
    }
  `,
  {
    props: ({mutate}) => ({
      login: (email, password) => mutate({variables: {email, password}}),
    }),
  },
)(Login);
