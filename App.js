/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import React, {Component} from 'react';
import Profile from './components/Profile';
import Homepage from './components/Homepage';
import Register from './components/Register';
import Login from './components/Login';
import {ApolloClient, HttpLink, InMemoryCache} from 'apollo-client-preset';
import {ApolloProvider} from 'react-apollo';
import {setContext} from 'apollo-link-context';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import 'apollo-client';
import 'apollo-link';

import {signIn, signOut, getToken} from './util';
GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest
const Stack = createStackNavigator();
const httpLink = new HttpLink({uri: 'https://api.foodstyles.com/graphql'});

const authLink = setContext(async (req, {headers}) => {
  const token = await getToken();

  return {
    ...headers,
    headers: {
      authorization: token ? `Bearer ${token}` : null,
    },
  };
});
const link = authLink.concat(httpLink);
const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
});

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loggedIn: false,
    };
  }

  async UNSAFE_componentWillMount() {
    const token = await getToken();
    if (token) {
      this.setState({loggedIn: true});
    }
  }

  handleChangeLoginState = (loggedIn = false, jwt) => {
    this.setState({loggedIn});
    if (loggedIn) {
      signIn(jwt);
    } else {
      signOut();
    }
  };

  render() {
    return (
      <ApolloProvider client={client}>
        <NavigationContainer>
          {!this.state.loggedIn ? (
            <Stack.Navigator initialRouteName="Home">
              <Stack.Screen
                name="Home"
                component={Homepage}
                screenProps={{changeLoginState: this.handleChangeLoginState}}
              />
              <Stack.Screen
                name="Register"
                component={Register}
                screenProps={{changeLoginState: this.handleChangeLoginState}}
              />
              <Stack.Screen
                name="Login"
                component={Login}
                screenProps={{changeLoginState: this.handleChangeLoginState}}
              />
            </Stack.Navigator>
          ) : (
            <Stack.Navigator>
              <Stack.Screen
                name="Profile"
                component={Profile}
                screenProps={{changeLoginState: this.handleChangeLoginState}}
              />
            </Stack.Navigator>
          )}
        </NavigationContainer>
      </ApolloProvider>
    );
  }
}
